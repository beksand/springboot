package pl.sda.springboot.springboot;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.springboot.springboot.dao.CustomerDAO;
import pl.sda.springboot.springboot.entities.Customer;

import java.util.List;

@Controller
public class TestController {

    @Autowired
    private CustomerDAO customerDAO;

    @RequestMapping(value = "/customer/add/{name:[A-Za-z0-9]+}/{address:[A-Za-z0-9]+}", method = RequestMethod.GET)
    public ModelAndView saveCustomer(@PathVariable("name") String name, @PathVariable("address") String address){

        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("name", name);
        modelAndView.addObject("address", address);
        Customer customer = new Customer();
        customer.setName(name);
        customer.setAddress(address);
        customerDAO.save(customer);
        return modelAndView;
    }
    @RequestMapping(value = "/customer/all", method = RequestMethod.GET)

    public ModelAndView loadBooks(){
        Iterable<Customer> customers = customerDAO.findAll();
        ModelAndView modelAndView = new ModelAndView("customers");
        modelAndView.addObject("customers", customers);
        return modelAndView;
    }
}
