package pl.sda.springboot.springboot.dao;

import org.springframework.data.repository.CrudRepository;
import pl.sda.springboot.springboot.entities.Book;

public interface BookDAO extends CrudRepository<Book, Long>{
}
