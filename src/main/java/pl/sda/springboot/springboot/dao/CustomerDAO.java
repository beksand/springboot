package pl.sda.springboot.springboot.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pl.sda.springboot.springboot.entities.Customer;

import java.util.List;

public interface CustomerDAO extends CrudRepository<Customer, Long> {
    @Query("select c from Customer c")
    List<Customer> getAllCustomer();
}
